public class ServiceCache {
    private IService service;

    // Private constructor to prevent external instantiation
    private ServiceCache() {
    }

    // Method to retrieve the service or create it if not available
    public IService getService() {
        if (service == null) {
            // Instantiate the IService if it's not already set
            service = createService(); // Replace this with your logic to create the IService instance
        }
        return service;
    }

    // Method to create the IService instance
    private IService createService() {
        return new MyServiceImplementation(); // Replace with your logic to create the IService instance
    }

    // Singleton instance getter
    public static ServiceCache getInstance() {
        return SingletonHolder.INSTANCE;
    }

    // Singleton holder to ensure lazy initialization
    private static class SingletonHolder {
        private static final ServiceCache INSTANCE = new ServiceCache();
    }
}
