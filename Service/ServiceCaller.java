public class MyServiceCaller {
    private IService service;

    public void performServiceCall() {
        // Get the IService instance from the ServiceCache (creates it if not already created)
        service = ServiceCache.getInstance().getService();

        // Synchronize the service invocation
        synchronized (service) {
            // Call the synchronized method in the IService implementation
            service.invokeServiceMethod();
        }
    }
}
